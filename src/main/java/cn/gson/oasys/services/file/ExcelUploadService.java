package cn.gson.oasys.services.file;

import cn.gson.oasys.model.dao.exceldao.ExcelDao;
import cn.gson.oasys.model.entity.excel.Excel;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;

@Service
public interface ExcelUploadService{

    public String readExcelFile(MultipartFile file);
}
