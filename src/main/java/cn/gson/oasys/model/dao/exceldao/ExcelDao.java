package cn.gson.oasys.model.dao.exceldao;

import cn.gson.oasys.model.entity.excel.Excel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ExcelDao extends JpaRepository<Excel,Integer> {
    @Override
    <S extends Excel> List<S> save(Iterable<S> iterable);

    @Override
    <S extends Excel> S save(S s);
}
