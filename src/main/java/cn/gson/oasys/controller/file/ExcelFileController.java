package cn.gson.oasys.controller.file;

import cn.gson.oasys.services.file.ExcelUploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/file")
public class ExcelFileController {

    @Autowired
    ExcelUploadService excelUploadService;
    @RequestMapping(value="/excel")
    public String excel(){
        return "file/Excel";
    }
    @RequestMapping(value="/upload",method = RequestMethod.POST)
    public String upload(MultipartFile file){
        System.err.println("我们能走到这里吗？");
        String flag=excelUploadService.readExcelFile(file);
        System.err.println(flag);
        if(flag.equals("2")){
            System.err.println("错误");
            return "a";
        }
        return "Excel";
    }
}
