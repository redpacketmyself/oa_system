package cn.gson.oasys;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import cn.gson.oasys.model.dao.exceldao.ExcelDao;
import cn.gson.oasys.model.dao.system.StatusDao;
import cn.gson.oasys.model.entity.excel.Excel;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.github.stuxuhai.jpinyin.PinyinException;
import com.github.stuxuhai.jpinyin.PinyinFormat;
import com.github.stuxuhai.jpinyin.PinyinHelper;

import cn.gson.oasys.mappers.NoticeMapper;
import cn.gson.oasys.model.dao.attendcedao.AttendceService;
import cn.gson.oasys.model.dao.processdao.NotepaperDao;
import cn.gson.oasys.model.dao.user.UserDao;

@SpringBootTest
@RunWith(SpringRunner.class)
public class Test1 {
	@Autowired
	private NotepaperDao notepaperDao;


	@Autowired
	ExcelDao excelDao;

	@Autowired
	StatusDao statusDao;

	@Autowired
	AttendceService attendceService;
	@Autowired
	UserDao uDao;

	@Test
	public void test(){
		System.out.println(uDao.findid("朱丽叶"));
	}
	
	@Test
	public void test2(){
		System.err.println(statusDao.findByStatusModel("aoa_task_list"));
		Scanner scan = new Scanner(System.in);
		scan.next();
		System.err.println(statusDao.findByStatusModel("aoa_mailnumber"));
		System.err.println(statusDao.findByStatusModel("aoa_mailnumber"));
	}
	/*@Test
	public void excelTest() throws Exception{
		HSSFWorkbook book;
		testReadExlPoi();
		try {
			book = new HSSFWorkbook(new FileInputStream(new File("E:/excel/blogs.xls")));
			System.out.println(book.getNameIndex("用户名"));
			HSSFSheet sheet=book.getSheet("用户名");
			//得到表格的第一行
			HSSFRow row=sheet.getRow(0);
			Iterator<Cell> ite=row.cellIterator();
			while(ite.hasNext()){
				HSSFCell cell=(HSSFCell)ite.next();
				String cname=cell.getStringCellValue();
				System.out.print(cname);
				System.out.print(",");
			}
			System.out.println("\n");
			System.out.println("---------------------------------------");
			//得到表格的其他行，即不包括第一行
			Iterator<Row> it= sheet.rowIterator();
			while(it.hasNext()){//遍历所有行
				row=(HSSFRow)it.next();
				ite=row.cellIterator();
				while(ite.hasNext()){//遍历当前行的所有列
					HSSFCell cell=(HSSFCell)ite.next();
					String cname=cell.getStringCellValue();
					System.out.print(cname);
					System.out.print(",");
				}
				System.out.println("\n");
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}*/
	 @Test
	public void testReadExlPoi() throws IOException {

		String filePath = "E:/excel/blogs.xls";

		// 获取文档
		HSSFWorkbook workbook = new HSSFWorkbook(new FileInputStream(filePath));
		// 获取表格
		HSSFSheet oneSheet = workbook.getSheetAt(0);
		 // 获取行
		 List<Excel> list = new ArrayList<>();

		for (Row row : oneSheet) {
			// 遍历每一行的列
			String[] val =new String  [3];
			int i=0;
			for (Cell cell : row) {
				// 获取每列中的数据
				// 获取类型
				int cellType = cell.getCellType();

				if (cellType == Cell.CELL_TYPE_NUMERIC) {
					// 数字类型
					double numericCellValue = cell.getNumericCellValue();
				} else if (cellType == Cell.CELL_TYPE_STRING) {
					// 字符串类型
					String value = cell.getStringCellValue();
					System.out.print(value + "\t");
				} else if (cellType == Cell.CELL_TYPE_FORMULA) {
					// 公式类型
					String value = cell.getCellFormula();

					System.out.print(value + "\t");
				}
				val[i]=cell.getStringCellValue();
				i++;
			}

			Excel excel = new Excel(val[0],val[1],val[2]);
			list.add(excel);
			//excelDao.save(excel);
			System.out.println();
		}
		excelDao.save(list);
	}

	
	

}
